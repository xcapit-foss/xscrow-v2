import { HardhatUserConfig } from 'hardhat/config';
import '@nomicfoundation/hardhat-toolbox';
import '@openzeppelin/hardhat-upgrades';
import '@nomicfoundation/hardhat-foundry';
require('./scripts/executeDeposit');
require('./scripts/partialWithdraw');
require('./scripts/deposit');
require('./scripts/depositTo');
require('./scripts/withdraw');
require('./scripts/depositWithPermit');
require('./scripts/signedDataDepositWithPermit');


const config: HardhatUserConfig = {
  solidity: {
    compilers: [{ version: '0.8.9' }, { version: '0.6.6' }, { version: '0.4.24' }],
  },
};

export default config;
