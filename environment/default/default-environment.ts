import { Environment } from '../environment.interface';
import { env } from 'process';
import dotenv from 'dotenv';

dotenv.config();
export default class DefaultEnvironment implements Environment {
  constructor(private readonly _env = env) {}

  public linkTokenAddress(): string {
    return this._env.LINK_TOKEN_ADDRESS!;
  }
  public operatorAddress(): string {
    return this._env.OPERATOR_ADDRESS!;
  }
  public jobId(): string {
    return this._env.JOB_ID!;
  }
  public withdrawApiUrl(): string {
    return this._env.WITHDRAW_API_URL!;
  }
  public executionApiUrl(): string {
    return this._env.EXECUTION_API_URL!;
  }
  public tokenAddress(): string {
    return this._env.TOKEN_ADDRESS!;
  }
  public lenderTreasuryAddress(): string {
    return this._env.LENDER_TREASURY_ADDRESS!;
  }
  public vendorTreasuryAddress(): string {
    return this._env.VENDOR_TREASURY_ADDRESS!;
  }
  public xscrowId(): string {
    return this._env.XSCROW_ID!;
  }
  public oracleAddress(): string {
    return this._env.ORACLE_ADDRESS!;
  }
  public minDepositAmount(): string {
    return this._env.MIN_DEPOSIT_AMOUNT!;
  }
  public maxDepositAmount(): string {
    return this._env.MAX_DEPOSIT_AMOUNT!;
  }

  public mnemonic(): string {
    return this._env.MNEMONIC!;
  }

  public rpcUrl(): string {
    return this._env.RPC_URL!;
  }
}
