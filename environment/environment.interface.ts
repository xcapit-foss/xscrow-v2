export interface Environment {
  linkTokenAddress(): string;
  operatorAddress(): string;
  jobId(): string;
  withdrawApiUrl(): string;
  executionApiUrl(): string;
  tokenAddress(): string;
  lenderTreasuryAddress(): string;
  vendorTreasuryAddress(): string;
  xscrowId(): string;
  oracleAddress(): string;
  minDepositAmount(): string;
  maxDepositAmount(): string;
  mnemonic(): string;
  rpcUrl(): string;
}
