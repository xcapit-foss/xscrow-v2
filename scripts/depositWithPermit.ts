import { task } from 'hardhat/config';
import { env } from 'process';
import { wallet } from './wallet';
import { PermitTypeValues } from './models/permit-type-values';
import { ComponentsOf } from './models/components-of';
import { DefaultTypedDataSignature } from './models/default-typed-data-signature';
import { EIP712Domain } from './models/EIP712-domain';
import { PermitTypeParams } from './models/permit-type-params';
import { Deadline } from './models/deadline';

import dotenv from 'dotenv';

dotenv.config();

const { RPC_URL, PRIVATE_KEY } = env;

task('depositWithPermit', 'Deposit token to the env wallet address')
  .addParam('xscrow', 'The xscrow contract address')
  .addParam('amount', 'A wei amount to deposit')
  .addParam('token', 'A token address to approve and deposit')
  .setAction(async (taskArgs, hre) => {
    const { xscrow, amount, token } = taskArgs;
    const iterableMapping = await (await hre.ethers.getContractFactory('IterableMapping')).deploy();
    const Xscrow = await hre.ethers.getContractFactory('Xscrow', {
      libraries: {
        IterableMapping: iterableMapping.address,
      },
    });

    const xscrowContract = Xscrow.attach(xscrow);
    const noEthWallet = wallet(hre.ethers);
    const FakeToken = await hre.ethers.getContractFactory('FakeToken');
    const fakeTokenContract = FakeToken.attach(token);
    const walletWithEth = new hre.ethers.Wallet(`0x${PRIVATE_KEY}`, new hre.ethers.providers.JsonRpcProvider(RPC_URL));

    const aPermitTypeValues = new PermitTypeValues(
      fakeTokenContract,
      noEthWallet,
      xscrowContract.address,
      hre.ethers.BigNumber.from(amount),
      new Deadline(4200)
    );

    const signatureComponents = new ComponentsOf(
      new DefaultTypedDataSignature(
        new EIP712Domain(fakeTokenContract, (await walletWithEth.provider!.getNetwork()).chainId),
        new PermitTypeParams(),
        aPermitTypeValues
      ),
      hre
    );

    console.log('Executing deposit with permit..');
    console.log('Signer: ', await noEthWallet.getAddress());
    console.log('Sponsor: ', walletWithEth.address);
    const requestDepositTx = await xscrowContract
      .connect(walletWithEth)
      .depositWithPermit(
        aPermitTypeValues.owner().address,
        aPermitTypeValues.amount(),
        aPermitTypeValues.deadline().toSeconds(),
        await signatureComponents.v(),
        await signatureComponents.r(),
        await signatureComponents.s()
      );
    console.log('Executing deposit...');
    await requestDepositTx.wait();
    console.log('Deposit successful!');
  });
