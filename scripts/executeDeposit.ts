import { wallet } from './wallet';
import { task } from 'hardhat/config';

task('executeDeposit', 'Execute a user deposit')
  .addParam('xscrow', 'The xscrow contract address')
  .addParam('payee', 'A payee address to execute deposit')
  .setAction(async (taskArgs, hre) => {
    const { xscrow, payee } = taskArgs;
    const Xscrow = await hre.ethers.getContractFactory('Xscrow');
    const xscrowContract = Xscrow.attach(xscrow);
    const _wallet = wallet(hre.ethers);
    const requestExecuteDepositTx = await xscrowContract.connect(_wallet).requestExecuteDepositOf(payee);
    console.log(`Executing deposit of address ${payee}`);
    await requestExecuteDepositTx.wait();
    console.log('Deposit executed!');
  });
