import { ethers } from 'hardhat';

export async function deployFakeToken() {
  const FakeToken = await ethers.getContractFactory('FakeToken');
  console.log('Deploying FakeToken...');
  const fakeToken = await FakeToken.deploy('FakeToken', 'FKT');
  return fakeToken;
}
