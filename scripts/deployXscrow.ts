import { ethers, upgrades } from 'hardhat';
import { Environment } from '../environment/environment.interface';
import DefaultEnvironment from '../environment/default/default-environment';
import { deployFakeToken } from './deployFakeToken';
import { wallet } from './wallet';
import { FakeToken } from '../typechain-types';

export async function deployXscrow(environment: Environment = new DefaultEnvironment()) {
  let token: FakeToken; 
  if (!environment.tokenAddress()) {
    token = await deployFakeToken();
    await token.mint(await wallet(ethers, environment).getAddress(), '50000000000000000000');
    console.log('FakeToken deployed');
  }else{
    const FakeToken = await ethers.getContractFactory('FakeToken');
    token = FakeToken.attach(environment.tokenAddress());
  }
  console.log("Token address: ", token.address);
  const iterableMapping = await (await ethers.getContractFactory('IterableMapping')).deploy();
  const CreditOracle = await ethers.getContractFactory('CreditOracle');
  console.log('Deploying CreditOracle...');
  const creditOracle = await CreditOracle.deploy(
    environment.linkTokenAddress(),
    environment.operatorAddress(),
    ethers.utils.toUtf8Bytes(environment.jobId()),
    environment.withdrawApiUrl(),
    environment.executionApiUrl()
  );
  console.log('CreditOracle deployed to:', creditOracle.address);
  const Xscrow = await ethers.getContractFactory('Xscrow', {
    libraries: {
      IterableMapping: iterableMapping.address,
    },
  });
  console.log('Deploying Xscrow...');
  const xscrow = await upgrades.deployProxy(
    Xscrow,
    [
      token.address,
      environment.lenderTreasuryAddress(),
      environment.vendorTreasuryAddress(),
      environment.xscrowId(),
      creditOracle.address,
      environment.minDepositAmount(),
      environment.maxDepositAmount(),
    ],
    { initializer: 'initialize', unsafeAllowLinkedLibraries: true }
  );
  await xscrow.deployed();
  console.log('Xscrow deployed to:', xscrow.address);
  await creditOracle.updateXscrow(xscrow.address);
  console.log('All set!');
  return {
    xscrow,
    creditOracle,
    token,
  };
}

deployXscrow();
