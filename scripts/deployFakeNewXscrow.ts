import { ethers } from 'hardhat';

export async function deployFakeNewXscrow() {
  const FakeNewXscrow = await ethers.getContractFactory('FakeNewXscrow');
  console.log('Deploying FakeNewXscrow');
  const fakeNewXscrow = await FakeNewXscrow.deploy();
  console.log('FakeNewXscrow deployed to:', fakeNewXscrow.address);
}

deployFakeNewXscrow();