import { task } from 'hardhat/config';
import { wallet } from './wallet';
import tokenAbi from './token-abi.json';

task('deposit', 'Deposit token to the env wallet address')
  .addParam('xscrow', 'The xscrow contract address')
  .addParam('amount', 'A wei amount to deposit')
  .addParam('token', 'A token address to approve and deposit')
  .setAction(async (taskArgs, hre) => {
    const { xscrow, amount, token } = taskArgs;
    const iterableMapping = await (await hre.ethers.getContractFactory('IterableMapping')).deploy();
    const Xscrow = await hre.ethers.getContractFactory('Xscrow', {
      libraries: {
        IterableMapping: iterableMapping.address,
      },
    });

    const xscrowContract = Xscrow.attach(xscrow);
    const _wallet = wallet(hre.ethers);
    const requestApproveTx = await new hre.ethers.Contract(token, tokenAbi, _wallet).approve(
      xscrowContract.address,
      amount
    );
    console.log('Requesting Approval...');
    await requestApproveTx.wait();
    console.log('Approved!');
    const requestDepositTx = await xscrowContract.connect(_wallet).deposit(amount);
    console.log('Executing deposit...');
    await requestDepositTx.wait();
    console.log('Deposit successful!');
  });
