import { task } from 'hardhat/config';
import { wallet } from './wallet';

task('withdraw', 'Execute a total withdraw of the user balance in the xscrow')
  .addParam('xscrow', 'The xscrow contract address')
  .setAction(async (taskArgs, hre) => {
    const { xscrow } = taskArgs;
    const Xscrow = await hre.ethers.getContractFactory('Xscrow');
    const xscrowContract = Xscrow.attach(xscrow);
    const _wallet = wallet(hre.ethers);
    const requestWithdrawTx = await xscrowContract.connect(_wallet).requestWithdraw();
    console.log(`Withdrawing total balance from configured wallet: ${await _wallet.getAddress()}`);
    await requestWithdrawTx.wait();
    console.log('Withdraw successful!');
  });
