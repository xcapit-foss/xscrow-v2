import { ethers } from 'hardhat';

export async function balanceOf() {
  const xscrow = '';
  const Xscrow = await ethers.getContractFactory('FakeNewXscrow');
  const xscrowContract = Xscrow.attach(xscrow);

  const wallets = ['', ''];

  for (let index = 0; index < wallets.length; index++) {
    const balance = await xscrowContract.balanceOf(wallets[index]);
    console.log(`Wallet: ${index} - balance: ${balance} wei`);
  }
}

balanceOf();
