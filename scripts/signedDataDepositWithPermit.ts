import { task } from 'hardhat/config';
import { wallet } from './wallet';
import { PermitTypeValues } from './models/permit-type-values';
import { ComponentsOf } from './models/components-of';
import { DefaultTypedDataSignature } from './models/default-typed-data-signature';
import { EIP712Domain } from './models/EIP712-domain';
import { PermitTypeParams } from './models/permit-type-params';
import { Deadline } from './models/deadline';

task('signedDataDepositWithPermit', 'Returns the values of a deposit with permit signed transaction')
  .addParam('xscrow', 'The xscrow contract address')
  .addParam('amount', 'A wei amount to deposit')
  .addParam('token', 'A token address to approve and deposit')
  .setAction(async (taskArgs, hre) => {
    const { xscrow, amount, token } = taskArgs;
    const iterableMapping = await (await hre.ethers.getContractFactory('IterableMapping')).deploy();
    const Xscrow = await hre.ethers.getContractFactory('Xscrow', {
      libraries: {
        IterableMapping: iterableMapping.address,
      },
    });
    const xscrowContract = Xscrow.attach(xscrow);
    const noMaticWallet = wallet(hre.ethers);
    const FakeToken = await hre.ethers.getContractFactory('FakeToken');
    const fakeTokenContract = FakeToken.attach(token);
    const chainId = 80001

    const aPermitTypeValues = new PermitTypeValues(
      fakeTokenContract,
      noMaticWallet,
      xscrowContract.address,
      hre.ethers.BigNumber.from(amount),
      new Deadline(4200)
    );

    const signatureComponents = new ComponentsOf(
      new DefaultTypedDataSignature(
        new EIP712Domain(fakeTokenContract, chainId),
        new PermitTypeParams(),
        aPermitTypeValues
      ),
      hre
    );

    console.log('Owner', await noMaticWallet.getAddress())
    console.log('Deadline', aPermitTypeValues.deadline().toSeconds())
    console.log('Amount',aPermitTypeValues.amount())
    console.log('v',await signatureComponents.v())
    console.log('r',await signatureComponents.r())
    console.log('s',await signatureComponents.s())
  });
