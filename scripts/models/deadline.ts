export class Deadline {

    constructor(
      private _inSeconds: number = 4200,
      private _fromADate: Date = new Date()
    ) {}
  
    toSeconds(): number {
      return Math.floor(this._fromADate.getTime() / 1000) + this._inSeconds;
    }
  }