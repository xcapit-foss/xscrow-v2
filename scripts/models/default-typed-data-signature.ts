import { EIP712Domain } from "./EIP712-domain";
import { PermitTypeParams } from "./permit-type-params";
import { PermitTypeValues } from "./permit-type-values";



export interface TypedDataSignature {
  value(): Promise<string>;
}


export class DefaultTypedDataSignature implements TypedDataSignature {
  constructor(
    private _aDomain: EIP712Domain,
    private _aPermitTypeParams: PermitTypeParams,
    private _aPermitTypeValues: PermitTypeValues
  ) {}

  async value(): Promise<string> {
    return await this._aPermitTypeValues
      .owner()
      ._signTypedData(
        await this._aDomain.toJSON(),
        this._aPermitTypeParams.value(),
        await this._aPermitTypeValues.value()
      );
  }
}
