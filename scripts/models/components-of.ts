import { HardhatRuntimeEnvironment } from "hardhat/types";
import { TypedDataSignature } from "./default-typed-data-signature";



export class ComponentsOf {
  private _cachedSignatureValue: any;

  constructor(private _aSignature: TypedDataSignature, private _hre: HardhatRuntimeEnvironment) {}

  async v(): Promise<string> {
    return (await this._signatureValue()).v;
  }

  async r(): Promise<string> {
    return (await this._signatureValue()).r;
  }

  async s(): Promise<string> {
    return (await this._signatureValue()).s;
  }

  private async _signatureValue(): Promise<any> {
    if (!this._cachedSignatureValue) {
      this._cachedSignatureValue = this._hre.ethers.utils.splitSignature(await this._aSignature.value());
    }
    return this._cachedSignatureValue;
  }
}
