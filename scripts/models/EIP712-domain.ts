import { FakeToken } from "../../typechain-types";



export type TypedDataDomain = {
    name?: string;
    version?: string;
    chainId?: number|string;
    verifyingContract?: string;
};

export class EIP712Domain {
  constructor(
    private _tokinContract: FakeToken,
    private _aChainId: number,
    private _aVersion: string = "1"
  ) {}

  async toJSON(): Promise<TypedDataDomain> {
    return {
      name: await this._tokinContract.name(),
      version: this._aVersion,
      chainId: this._aChainId,
      verifyingContract: this._tokinContract.address,
    };
  }
}
