# Scripts/utils

## Xscrow scripts

### deposit

npx hardhat deposit --xscrow <xscrow_contract_address> --amount <wei_amount_to_deposit> --token <warranty_token>

### depositTo

npx hardhat depositTo --payee <payee_address> --xscrow <xscrow_contract_address> --amount <wei_amount_to_deposit> --token <warranty_token>

### executeDeposit

npx hardhat executeDeposit --xscrow <xscrow_contract_address> --payee <payee_address_to_execute>

### withdraw

npx hardhat withdraw --xscrow <xscrow_contract_address>

### partialWithdraw

npx hardhat partialWithdraw --xscrow <xscrow_contract_address> --amount <wei_amount_to_deposit>
