import { env } from 'process';
import { ethers } from 'hardhat';
import tokenAbi from './token-abi.json';
import xscrowAbi from './xscrow-abi.json';
import dotenv from 'dotenv';

dotenv.config();


const testTimeOut = async () => {
  const { RPC_URL, PRIVATE_KEY, TOKEN_ADDRESS } = env;
  const token = TOKEN_ADDRESS!;
  const xscrow = '';
  const amountOfWallets = 10;
  const testAmount = 10000000;
  const testForFee = 773789644303186;
  const provider = new ethers.providers.JsonRpcProvider(RPC_URL);
  const faucetWallet = new ethers.Wallet(`0x${PRIVATE_KEY}`).connect(provider);
  console.log('Faucet Wallet:', faucetWallet.address);
  for (let index = 0; index < amountOfWallets; index++) {
    const wallet = ethers.Wallet.createRandom().connect(provider);
    console.log('Wallet:', wallet.address, index);
    const requestFeeTx = await faucetWallet.sendTransaction({ to: wallet.address, value: testForFee });
    await requestFeeTx.wait();
    const requestTransferTx = await new ethers.Contract(token, tokenAbi, faucetWallet).transfer(wallet.address, testAmount);
    await requestTransferTx.wait();
    const requestApproveTx = await new ethers.Contract(token, tokenAbi, wallet).approve(xscrow, testAmount);
    await requestApproveTx.wait();
    const requestDepositTx = await new ethers.Contract(xscrow, xscrowAbi, wallet).deposit(testAmount);
    await requestDepositTx.wait();
    console.log('Warranty created!');
  }
};

testTimeOut();
