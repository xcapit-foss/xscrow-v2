import { Signer } from 'ethers';
import DefaultEnvironment from '../environment/default/default-environment';
import { Environment } from '../environment/environment.interface';

export const wallet = (ethers: any, environment: Environment = new DefaultEnvironment()): Signer => {
  const wallet = ethers.Wallet.fromMnemonic(environment.mnemonic());
  return wallet.connect(new ethers.providers.JsonRpcProvider(environment.rpcUrl()));
};
