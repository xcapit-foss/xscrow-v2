import { task } from 'hardhat/config';
import { wallet } from './wallet';

task('partialWithdraw', 'Execute a partial withdraw of the user balance in the xscrow')
  .addParam('xscrow', 'The xscrow contract address')
  .addParam('amount', 'Amount to withdraw')
  .setAction(async (taskArgs, hre) => {
    const { xscrow, amount } = taskArgs;
    const Xscrow = await hre.ethers.getContractFactory('Xscrow');
    const xscrowContract = Xscrow.attach(xscrow);
    const _wallet = wallet(hre.ethers);
    const requestPartialWithdrawTx = await xscrowContract.connect(_wallet).requestPartialWithdraw(amount);
    console.log(`Withdrawing ${amount} from configured wallet: ${await _wallet.getAddress()}`);
    await requestPartialWithdrawTx.wait();
    console.log('Partial withdraw successful!');
  });
