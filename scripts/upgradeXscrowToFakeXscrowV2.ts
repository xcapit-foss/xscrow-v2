import { env } from 'process';
import dotenv from 'dotenv';
import { ethers, upgrades } from 'hardhat';

dotenv.config();
const { PROXY_ADDRESS } = env;

async function main() {
  const FakeXscrowV2 = await ethers.getContractFactory('FakeXscrowV2');
  console.log('Upgrading FakeXscrowV2...');
  const xscrow = await upgrades.upgradeProxy(`0x${PROXY_ADDRESS!}`, FakeXscrowV2);
  await xscrow.deployed();
  console.log('FakeXscrowV2 upgraded');
}

main();
