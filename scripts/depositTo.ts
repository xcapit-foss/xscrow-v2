import { task } from 'hardhat/config';
import { wallet } from './wallet';
import tokenAbi from './token-abi.json';

task('depositTo', 'Deposit token for a Payee')
  .addParam('xscrow', 'The xscrow contract address')
  .addParam('amount', 'A wei amount to deposit')
  .addParam('token', 'A token address to approve and deposit')
  .addParam('payee', 'A payee address to deposit on behalf of')
  .setAction(async (taskArgs, hre) => {
    const { xscrow, amount, token, payee } = taskArgs;
    const Xscrow = await hre.ethers.getContractFactory('Xscrow');
    const xscrowContract = Xscrow.attach(xscrow);
    const _wallet = wallet(hre.ethers);
    const requestApproveTx = await new hre.ethers.Contract(token, tokenAbi, _wallet).approve(
      xscrowContract.address,
      amount
    );
    console.log('Requesting Approval...');
    await requestApproveTx.wait();
    console.log('Approved!');
    const requestDepositTx = await xscrowContract.connect(_wallet).depositTo(payee,amount);
    console.log(`Depositing on behalf of ${payee} ...`)
    await requestDepositTx.wait();
    console.log('Deposit successful!');
  });
