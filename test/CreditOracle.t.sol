// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "forge-std/Test.sol";
import "../contracts/fakes/FakeXscrow.sol";
import "../contracts/fakes/FakeToken.sol";
import "../contracts/fakes/FakeOperator.sol";
import "../contracts/CreditOracle.sol";
import "@chainlink/contracts/src/v0.8/interfaces/LinkTokenInterface.sol";
import "../contracts/fakes/LinkToken.sol";

contract CreditOracleTest is Test {
    event XscrowUpdated(address indexed aXscrow);
    event RequestPartialWithdrawOf(address indexed aPayee, uint256 anAmount);
    event DataFulfilled(address indexed aPayee, bool allowed);
    event RequestWithdrawOf(address indexed aPayee, uint256 anAmount);
    event RequestExecuteDepositOf(address indexed aPayee, uint256 anAmount);

    CreditOracle creditOracle;
    FakeXscrow fakeXscrow;
    FakeToken fakeToken;
    LinkToken linkToken;
    FakeOperator fakeOperator;
    address wallet1 = address(1);
    uint256 withdrawAmount = 1000;
    uint256 partialWithdrawAmount = 1000;

    function setUp() public {
        fakeToken = new FakeToken("LINK", "LINK");
        linkToken = new LinkToken();
        fakeOperator = new FakeOperator(address(linkToken));
        creditOracle = new CreditOracle(
            LinkTokenInterface(address(linkToken)),
            address(fakeOperator),
            "",
            "http://withdrawurl.com",
            "http://executionurl.com"
        );
        fakeXscrow = new FakeXscrow(address(wallet1));
        linkToken.transfer(address(creditOracle), 10 ether);
    }

    function test_revertWithAddressZeroNotAllowed_linkToken() public {
        vm.expectRevert(CreditOracle.AddressZeroNotAllowed.selector);
        new CreditOracle(
            LinkTokenInterface(address(0)),
            address(fakeOperator),
            "",
            "http://withdrawurl.com",
            "http://executionurl.com"
        );
    }

    function test_revertWithAddressZeroNotAllowed_operator() public {
        vm.expectRevert(CreditOracle.AddressZeroNotAllowed.selector);
        new CreditOracle(
            LinkTokenInterface(address(fakeToken)),
            address(0),
            "",
            "http://withdrawurl.com",
            "http://executionurl.com"
        );
    }

    function test_updateXscrow() public {
        creditOracle.updateXscrow(fakeXscrow);
        assertEq(creditOracle.xscrow(), address(fakeXscrow));
    }

    function test_revertWithNotXscrow_requestExecuteDepositOf() public {
        creditOracle.updateXscrow(fakeXscrow);
        vm.expectRevert(CreditOracle.NotXscrow.selector);
        creditOracle.requestExecuteDepositOf(wallet1, withdrawAmount);
    }

    function test_revertWithNotXscrow_requestWithdrawOf() public {
        creditOracle.updateXscrow(fakeXscrow);
        vm.expectRevert(CreditOracle.NotXscrow.selector);
        creditOracle.requestWithdrawOf(wallet1, withdrawAmount);
    }

    function test_revertWithNotXscrow_requestPartialWithdrawOf() public {
        creditOracle.updateXscrow(fakeXscrow);
        vm.expectRevert(CreditOracle.NotXscrow.selector);
        creditOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);
    }

    function test_revertWithAddressZeroNotAllowed_updateXscrow() public {
        vm.expectRevert(CreditOracle.AddressZeroNotAllowed.selector);
        creditOracle.updateXscrow(FakeXscrow(address(0)));
    }

    function test_revertWithAddressZeroNotAllowed_updateOperator() public {
        vm.expectRevert(CreditOracle.AddressZeroNotAllowed.selector);
        creditOracle.updateOperator(address(0));
    }

    function test_emitRequestWithdrawOf_requestWithdrawOf() public {
        creditOracle.updateXscrow(fakeXscrow);
        vm.expectEmit();
        emit RequestWithdrawOf(wallet1, partialWithdrawAmount);
        vm.prank(address(fakeXscrow));
        creditOracle.requestWithdrawOf(wallet1, partialWithdrawAmount);
    }

    function test_emitRequestPartialWithdrawOf_requestPartialWithdrawOf()
        public
    {
        creditOracle.updateXscrow(fakeXscrow);
        vm.expectEmit();
        emit RequestPartialWithdrawOf(wallet1, partialWithdrawAmount);
        vm.prank(address(fakeXscrow));
        creditOracle.requestPartialWithdrawOf(wallet1, partialWithdrawAmount);
    }

    function test_emitDataFulfilled_fulfillOracleRequest() public {
        vm.recordLogs();
        creditOracle.updateXscrow(fakeXscrow);
        vm.prank(address(fakeXscrow));
        creditOracle.requestWithdrawOf(wallet1, withdrawAmount);

        Vm.Log[] memory logs = vm.getRecordedLogs();
        bytes32 reqId = logs[2].topics[1];

        vm.expectEmit();
        emit DataFulfilled(wallet1, true);
        vm.prank(address(fakeXscrow));
        fakeOperator.fulfillOracleRequest(reqId, bytes32(uint256(1)));
    }

    function test_emitDataFulfilled_fulfillOracleRequest_requestWithdrawOf()
        public
    {
        vm.recordLogs();
        creditOracle.updateXscrow(fakeXscrow);
        vm.prank(address(fakeXscrow));
        creditOracle.requestWithdrawOf(wallet1, withdrawAmount);

        Vm.Log[] memory logs = vm.getRecordedLogs();
        bytes32 reqId = logs[2].topics[1];

        vm.expectEmit();
        emit DataFulfilled(wallet1, true);
        fakeOperator.fulfillOracleRequest(reqId, bytes32(uint256(1)));
    }

    function test_emitDataFulfilled_fulfillOracleRequest_requestPartialWithdrawOf()
        public
    {
        vm.recordLogs();
        creditOracle.updateXscrow(fakeXscrow);
        vm.prank(address(fakeXscrow));
        creditOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);

        Vm.Log[] memory logs = vm.getRecordedLogs();
        bytes32 reqId = logs[2].topics[1];

        vm.expectEmit();
        emit DataFulfilled(wallet1, true);
        fakeOperator.fulfillOracleRequest(reqId, bytes32(uint256(1)));
    }

    function test_emitRequestExecuteDepositOf_requestExecuteDepositOf() public {
        creditOracle.updateXscrow(fakeXscrow);
        vm.expectEmit();
        emit RequestExecuteDepositOf(wallet1, partialWithdrawAmount);
        vm.prank(address(fakeXscrow));
        creditOracle.requestExecuteDepositOf(wallet1, partialWithdrawAmount);
    }

    function test_emitDataFulfilled_fulfillOracleRequest_requestExecuteDepositOf()
        public
    {
        vm.recordLogs();
        creditOracle.updateXscrow(fakeXscrow);
        vm.prank(address(fakeXscrow));
        creditOracle.requestExecuteDepositOf(wallet1, withdrawAmount);

        Vm.Log[] memory logs = vm.getRecordedLogs();
        bytes32 reqId = logs[2].topics[1];

        vm.expectEmit();
        emit DataFulfilled(wallet1, true);
        fakeOperator.fulfillOracleRequest(reqId, bytes32(uint256(1)));
    }

    function test_emitXscrowUpdated_updateXscrow() public {
        vm.expectEmit();
        emit XscrowUpdated(address(fakeXscrow));
        creditOracle.updateXscrow(fakeXscrow);
    }
}
