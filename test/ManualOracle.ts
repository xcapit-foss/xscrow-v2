import { ethers } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { expect } from 'chai';
import { FakeXscrow, ManualOracle } from '../typechain-types';
import { BigNumber } from 'ethers';

describe.skip('ManualOracle', () => {
  ethers.utils.Logger.setLogLevel(ethers.utils.Logger.levels.ERROR);

  let oracle: ManualOracle;
  let fakeXscrow: FakeXscrow;
  let owner: SignerWithAddress;
  let wallet1: SignerWithAddress;

  const xscrowRejectedMsg = 'Caller is not the xscrow';
  const ownableRejectedMsg = 'Ownable: caller is not the owner';
  const requestNotFoundMsg = 'Request not found';
  const alreadyRequestedMsg = 'Already requested';
  const zeroAddressMsg = 'Address can not be zero';

  const withdrawAmount = 1000;
  const executionAmount = 1000;

  beforeEach(async () => {
    [owner, wallet1] = await ethers.getSigners();
    oracle = await (await ethers.getContractFactory('ManualOracle')).deploy();
    fakeXscrow = await (await ethers.getContractFactory('FakeXscrow')).deploy(oracle.address);

    await oracle.updateXscrow(fakeXscrow.address);
  });

  describe('deploy settings', () => {
    it('new', async () => {
      expect(oracle).to.not.be.null;
    });
  });

  describe('updatable properties', () => {
    it('owner can update xscrow address', async () => {
      await oracle.updateXscrow(wallet1.address);
      expect(await oracle.xscrow()).to.equal(wallet1.address);
    });

    it('not owner cannot update xscrow', async () => {
      await expect(oracle.connect(wallet1).updateXscrow(wallet1.address)).to.revertedWith(ownableRejectedMsg);
    });

    it('update xscrow event', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.XscrowUpdated(wallet1.address);
        oracle.once(filter, (aXscrow: any) => {
          expect(wallet1.address).to.equal(aXscrow);
          resolve(true);
        });

        await oracle.updateXscrow(wallet1.address);
      });
    });

    it('not zero address on update xscrow', async () => {
      await expect(oracle.updateXscrow(ethers.constants.AddressZero)).to.revertedWith(zeroAddressMsg);
    });
  });

  describe('Partial Withdraw', () => {
    it('requestPartialWithdrawOf', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestPartialWithdrawOf(wallet1.address, withdrawAmount);
      const transactionReceipt = await transaction.wait(1);
      const requestId = transactionReceipt.events![0].topics[1];
      const partialWithdrawRequest = await oracle.withdrawRequestOf(wallet1.address);

      expect(requestId).to.not.be.null;
      expect(partialWithdrawRequest).to.equal(withdrawAmount);
    });

    it('requestPartialWithdrawOf event', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.RequestPartialWithdrawOf(wallet1.address);
        oracle.once(filter, (aPayee: any, anAmount: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(withdrawAmount).to.equal(anAmount);
          resolve(true);
        });

        await oracle.updateXscrow(owner.address);
        await (await oracle.requestPartialWithdrawOf(wallet1.address, withdrawAmount)).wait(1);
      });
    });

    it('requestPartialWithdrawOf twice', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestPartialWithdrawOf(wallet1.address, 1000);
      await transaction.wait(1);

      await expect(oracle.requestPartialWithdrawOf(wallet1.address, 1000)).to.revertedWith(alreadyRequestedMsg);
    });

    it('requestPartialWithdrawOf not xscrow', async () => {
      await expect(oracle.connect(wallet1).requestPartialWithdrawOf(wallet1.address, withdrawAmount)).to.revertedWith(
        xscrowRejectedMsg
      );
    });

    it('fulfillPartialWithdraw without request', async () => {
      await expect(oracle.fulfillPartialWithdraw(wallet1.address, true)).to.revertedWith(requestNotFoundMsg);
    });

    it('fulfillPartialWithdraw through fake oracle from xscrow', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.DataFulfilled(wallet1.address);
        oracle.once(filter, (aPayee: any, allowed: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(allowed).to.equal(true);
          resolve(true);
        });

        await (await fakeXscrow.connect(wallet1).requestPartialWithdraw(1000)).wait(1);
        await oracle.fulfillPartialWithdraw(wallet1.address, true);
      });
    });

    it('fulfillPartialWithdraw not owner', async () => {
      await expect(oracle.connect(wallet1).fulfillPartialWithdraw(wallet1.address, true)).to.revertedWith(
        ownableRejectedMsg
      );
    });
  });

  describe('Withdraw', () => {
    it('withdrawRequestOf', async () => {
      await (await fakeXscrow.connect(wallet1).requestWithdraw()).wait(1);
      expect(await oracle.withdrawRequestOf(wallet1.address)).to.not.null;
    });

    it('withdrawRequestOf not owner', async () => {
      await expect(oracle.connect(wallet1).withdrawRequestOf(wallet1.address)).to.revertedWith(ownableRejectedMsg);
    });

    it('withdrawRequestOf without request', async () => {
      expect(await oracle.withdrawRequestOf(wallet1.address)).to.equal(BigNumber.from(0));
    });

    it('requestWithdrawOf', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestWithdrawOf(wallet1.address, withdrawAmount);
      const transactionReceipt = await transaction.wait(1);
      const requestId = transactionReceipt.events![0].topics[1];
      const withdrawRequest = await oracle.withdrawRequestOf(wallet1.address);

      expect(requestId).to.not.be.null;
      expect(withdrawRequest).to.equal(BigNumber.from(withdrawAmount));
    });

    it('requestWithdrawOf event', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.RequestWithdrawOf(wallet1.address);
        oracle.once(filter, (aPayee: any, anAmount: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(withdrawAmount).to.equal(anAmount);
          resolve(true);
        });

        await oracle.updateXscrow(owner.address);
        await (await oracle.requestWithdrawOf(wallet1.address, withdrawAmount)).wait(1);
      });
    });

    it('requestWithdrawOf twice', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestWithdrawOf(wallet1.address, withdrawAmount);
      await transaction.wait(1);

      await expect(oracle.requestWithdrawOf(wallet1.address, withdrawAmount)).to.revertedWith(alreadyRequestedMsg);
    });

    it('requestWithdrawOf not xscrow', async () => {
      await expect(oracle.connect(wallet1).requestWithdrawOf(wallet1.address, withdrawAmount)).to.revertedWith(
        xscrowRejectedMsg
      );
    });
    
    it('fulfillWithdraw without request', async () => {
      await expect(oracle.fulfillWithdraw(wallet1.address, true)).to.revertedWith(requestNotFoundMsg);
    });
    
    it('fulfillWithdraw through fake oracle from xscrow', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.DataFulfilled(wallet1.address);
        oracle.once(filter, (aPayee: any, allowed: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(allowed).to.equal(true);
          resolve(true);
        });

        await (await fakeXscrow.connect(wallet1).requestWithdraw()).wait(1);
        await oracle.fulfillWithdraw(wallet1.address, true);
      });
    });

    it('fulfillWithdraw not owner', async () => {
      await expect(oracle.connect(wallet1).fulfillWithdraw(wallet1.address, true)).to.revertedWith(ownableRejectedMsg);
    });
  });

  describe('Execute Deposit', () => {
    
    it('executionRequestOf', async () => {
      await (await fakeXscrow.requestExecuteDepositOf(wallet1.address)).wait(1);
      expect(await oracle.executionRequestOf(wallet1.address)).to.not.null;
    });
    
    it('executionRequestOf not owner', async () => {
      await expect(oracle.connect(wallet1).executionRequestOf(wallet1.address)).to.revertedWith(ownableRejectedMsg);
    });
    
    it('executionRequestOf without request', async () => {
      expect(await oracle.executionRequestOf(wallet1.address)).to.equal(BigNumber.from(0));
    });
    

    it('requestExecuteDepositOf', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestExecuteDepositOf(wallet1.address, executionAmount);
      const transactionReceipt = await transaction.wait(1);
      const requestId = transactionReceipt.events![0].topics[1];
      const executionRequest = await oracle.executionRequestOf(wallet1.address);

      expect(requestId).to.not.be.null;
      expect(executionRequest).to.equal(BigNumber.from(executionAmount));
    });


    it('requestExecuteDepositOf event', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.RequestExecuteDepositOf(wallet1.address);
        oracle.once(filter, (aPayee: any, anAmount: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(executionAmount).to.equal(anAmount);
          resolve(true);
        });

        await oracle.updateXscrow(owner.address);
        await (await oracle.requestExecuteDepositOf(wallet1.address, executionAmount)).wait(1);
      });
    });


    it('requestExecuteDepositOf twice', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestExecuteDepositOf(wallet1.address, executionAmount);
      await transaction.wait(1);

      await expect(oracle.requestExecuteDepositOf(wallet1.address, executionAmount)).to.revertedWith(
        alreadyRequestedMsg
      );
    });

    it('requestExecuteDepositOf not xscrow', async () => {
      await expect(oracle.connect(wallet1).requestExecuteDepositOf(wallet1.address, executionAmount)).to.revertedWith(
        xscrowRejectedMsg
      );
    });

    it('fulfillExecution without request', async () => {
      await expect(oracle.fulfillExecution(wallet1.address, true)).to.revertedWith(requestNotFoundMsg);
    });

    it('fulfillExecution through fake oracle from xscrow', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.DataFulfilled(wallet1.address);
        oracle.once(filter, (aPayee: any, allowed: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(allowed).to.equal(true);
          resolve(true);
        });

        await (await fakeXscrow.connect(wallet1).requestExecuteDepositOf(wallet1.address)).wait(1);
        await oracle.fulfillExecution(wallet1.address, true);
      });
    });

    it('fulfillExecution not owner', async () => {
      await expect(oracle.connect(wallet1).fulfillExecution(wallet1.address, true)).to.revertedWith(ownableRejectedMsg);
    });
  });
});
