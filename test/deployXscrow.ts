import { deployXscrow } from '../scripts/deployXscrow';
import { expect } from 'chai';
import { Environment } from '../environment/environment.interface';
import DefaultEnvironment from '../environment/default/default-environment';
import { rawEnvironmentData } from '../environment/raw/raw-environment-data';
import sinon from 'sinon';
import { ethers } from 'hardhat';
import { CreditOracle } from '../typechain-types';
import { FakeToken } from '../typechain-types';

describe('deployXscrow', () => {
  let fakeEnv: Environment;
  let xscrow: any;
  let creditOracle: CreditOracle;
  let fakeToken: FakeToken;

  before(() => {
    sinon.replace(console, 'log', sinon.fake());
  });

  beforeEach(async () => {
    fakeEnv = new DefaultEnvironment(rawEnvironmentData);
    const result = await deployXscrow(fakeEnv);
    xscrow = result.xscrow;
    creditOracle = result.creditOracle;
    fakeToken = result.token;
  });

  describe('xscrow', () => {
    it('tokenAddress', async () => {
      expect(await xscrow.tokenAddress()).to.equal(fakeToken.address);
    });

    it('lenderTreasuryAddress', async () => {
      expect(await xscrow.lenderTreasury()).to.equal(fakeEnv.lenderTreasuryAddress());
    });

    it('vendorTreasuryAddress', async () => {
      expect(await xscrow.vendorTreasury()).to.equal(fakeEnv.vendorTreasuryAddress());
    });

    it('oracle', async () => {
      expect(await xscrow.oracle()).to.equal(creditOracle.address);
    });

    it('identifier', async () => {
      expect(await xscrow.identifier()).to.equal(fakeEnv.xscrowId());
    });

    it('depositFee', async () => {
      expect(await xscrow.depositFee()).to.equal(0);
    });

    it('balanceOf', async () => {
      expect(await xscrow.balanceOf(fakeEnv.lenderTreasuryAddress())).to.equal(0);
    });

    it('minimumDepositAmount', async () => {
      expect(await xscrow.minimumDepositAmount()).to.equal(fakeEnv.minDepositAmount());
    });

    it('maximumDepositAmount', async () => {
      expect(await xscrow.maximumDepositAmount()).to.equal(fakeEnv.maxDepositAmount());
    });
  });

  describe('creditOracle', () => {
    it('xscrow address', async () => {
      expect(await creditOracle.xscrow()).to.equal(xscrow.address);
    });

    it('withdrawApiUrl', async () => {
      expect(await creditOracle.withdrawApiUrl()).to.equal(fakeEnv.withdrawApiUrl());
    });

    it('executionApiUrl', async () => {
      expect(await creditOracle.executionApiUrl()).to.equal(fakeEnv.executionApiUrl());
    });

    it('jobId', async () => {
      expect(ethers.utils.toUtf8String(await creditOracle.jobId())).to.equal(fakeEnv.jobId());
    });

    it('operator', async () => {
      expect(await creditOracle.operator()).to.equal(fakeEnv.operatorAddress());
    });
  });
});
