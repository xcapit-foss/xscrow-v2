import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { FakeCreditOracle, FakeToken, FakeXscrowV2, Xscrow } from '../typechain-types';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

describe(`Upgrade Xscrow to FakeXscrowV2`, () => {
  let xscrow: Xscrow;
  let fakeXscrowV2: FakeXscrowV2;
  let owner: SignerWithAddress;
  let wallet1: SignerWithAddress;
  let wallet2: SignerWithAddress;
  let lenderTreasury: SignerWithAddress;
  let anotherLenderTreasury: SignerWithAddress;
  let libIterableMapping: any;
  let vendorTreasury: SignerWithAddress;
  let oracle: FakeCreditOracle;
  let fakeToken: FakeToken;

  const testFee = 2;
  const testAmount = 10000000;
  const testFeeAmount = testAmount * (testFee / 100);
  const aDummyValue = 3;
  const ownableRejectedMsg = 'Ownable: caller is not the owner';
  const testIdentifier = 'token_xscrow';
  const minimumDepositAmount = 1000000;
  const maximumDepositAmount = 10000000;

  const _changeStorageValues = async () => {
    await xscrow.updateLenderTreasury(anotherLenderTreasury.address);
    await fakeToken.mint(wallet1.address, testAmount);
    await fakeToken.connect(wallet1).approve(xscrow.address, testAmount);
    await xscrow.connect(wallet1).deposit(testAmount);
  };

  beforeEach(async () => {
    [owner, wallet1, wallet2, lenderTreasury, vendorTreasury, anotherLenderTreasury] = await ethers.getSigners();

    fakeToken = await (await ethers.getContractFactory('FakeToken')).deploy('TokenX', 'TKX');
    libIterableMapping = await (await ethers.getContractFactory('IterableMapping')).deploy();
    oracle = await (await ethers.getContractFactory('FakeCreditOracle')).deploy();
    xscrow = (await upgrades.deployProxy(
      await ethers.getContractFactory('Xscrow', {
        libraries: {
          IterableMapping: libIterableMapping.address,
        },
      }),
      [
        fakeToken.address,
        lenderTreasury.address,
        vendorTreasury.address,
        testIdentifier,
        oracle.address,
        minimumDepositAmount,
        maximumDepositAmount,
      ],
      { initializer: 'initialize', kind: 'uups', unsafeAllowLinkedLibraries: true }
    )) as Xscrow;

    await _changeStorageValues();

    fakeXscrowV2 = (await upgrades.upgradeProxy(
      xscrow.address,
      await ethers.getContractFactory('FakeXscrowV2', {
        libraries: {
          IterableMapping: libIterableMapping.address,
        },
      })
    , {unsafeAllowLinkedLibraries: true})) as FakeXscrowV2;

    await fakeXscrowV2.updateDepositFee(testFee);
  });

  describe('Settings & storage after a contract upgrade', () => {
    it('should not initialize twice', async () => {
      await expect(
        fakeXscrowV2.initialize(
          fakeToken.address,
          lenderTreasury.address,
          vendorTreasury.address,
          testIdentifier,
          oracle.address,
          minimumDepositAmount,
          maximumDepositAmount
        )
      ).to.revertedWith('Initializable: contract is already initialized');
    });

    it('ownership', async () => {
      expect(await fakeXscrowV2.owner()).to.be.equal(owner.address);
    });

    it('token address', async () => {
      expect(await fakeXscrowV2.tokenAddress()).to.be.equal(fakeToken.address);
    });

    it('identifier', async () => {
      expect(await fakeXscrowV2.identifier()).to.be.equal(testIdentifier);
    });

    it('oracle', async () => {
      expect(await fakeXscrowV2.oracle()).to.be.equal(oracle.address);
    });

    it('lender treasury', async () => {
      expect(await fakeXscrowV2.lenderTreasury()).to.be.equal(anotherLenderTreasury.address);
    });

    it('balance of with storage changes before and after contract upgrade', async () => {
      await fakeToken.mint(wallet2.address, testAmount);
      await fakeToken.connect(wallet2).approve(fakeXscrowV2.address, testAmount);
      await fakeXscrowV2.connect(wallet2).deposit(testAmount);

      expect(await fakeXscrowV2.balanceOf(wallet1.address)).to.be.equal(testAmount);
      expect(await fakeXscrowV2.balanceOf(wallet2.address)).to.be.equal(testAmount - testFeeAmount);
    });

    it('owner set dummy', async () => {
      await fakeXscrowV2.setDummy(aDummyValue);

      expect(await fakeXscrowV2.dummy()).to.equal(aDummyValue);
    });

    it('only owner can set dummy', async () => {
      await expect(fakeXscrowV2.connect(wallet1).setDummy(aDummyValue)).to.be.rejectedWith(ownableRejectedMsg);
    });
  });
});
