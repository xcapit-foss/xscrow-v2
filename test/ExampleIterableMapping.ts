import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { expect } from 'chai';
import { ethers } from 'hardhat';
import { ExampleIterableMapping } from '../typechain-types';

describe('ExampleIterableMapping', () => {
  let exampleIterableMapping: ExampleIterableMapping;
  let libIterableMapping: any;
  let owner: SignerWithAddress;
  let wallet1: SignerWithAddress;
  let wallet2: SignerWithAddress;

  const aTestWarranty = { amount: 1, created_at: 2, updated_at: 3 };

  beforeEach(async () => {
    libIterableMapping = await (await ethers.getContractFactory('IterableMapping')).deploy();
    exampleIterableMapping = await (
      await ethers.getContractFactory('ExampleIterableMapping', {
        libraries: {
          IterableMapping: libIterableMapping.address,
        },
      })
    ).deploy();
    [owner, wallet1, wallet2] = await ethers.getSigners();
  });

  it('new', () => {
    expect(exampleIterableMapping).to.not.be.null;
  });

  it('get and set', async () => {
    await exampleIterableMapping.set(owner.address, aTestWarranty);

    const warranty = await exampleIterableMapping.get(owner.address);

    expect(warranty.amount).to.equal(aTestWarranty.amount);
    expect(warranty.created_at).to.equal(aTestWarranty.created_at);
    expect(warranty.updated_at).to.equal(aTestWarranty.updated_at);
  });

  it('set update', async () => {
    await exampleIterableMapping.set(owner.address, aTestWarranty);
    await exampleIterableMapping.set(owner.address, { ...aTestWarranty, amount: 7 });

    const warranty = await exampleIterableMapping.get(owner.address);

    expect(warranty.amount).to.equal(7);
    expect(warranty.created_at).to.equal(aTestWarranty.created_at);
    expect(warranty.updated_at).to.equal(aTestWarranty.updated_at);
  });

  it('size', async () => {
    await exampleIterableMapping.set(owner.address, aTestWarranty);
    await exampleIterableMapping.set(wallet1.address, aTestWarranty);
    await exampleIterableMapping.set(wallet2.address, aTestWarranty);

    expect(await exampleIterableMapping.size()).to.equal(3);
  });

  it('getKeyAtIndex', async () => {
    await exampleIterableMapping.set(owner.address, aTestWarranty);
    await exampleIterableMapping.set(wallet1.address, aTestWarranty);

    expect(await exampleIterableMapping.getKeyAtIndex(0)).to.equal(owner.address);
    expect(await exampleIterableMapping.getKeyAtIndex(1)).to.equal(wallet1.address);
  });

  it('remove', async () => {
    await exampleIterableMapping.set(owner.address, aTestWarranty);
    await exampleIterableMapping.set(wallet1.address, aTestWarranty);
    await exampleIterableMapping.set(wallet2.address, aTestWarranty);

    await exampleIterableMapping.remove(wallet1.address);

    expect(await exampleIterableMapping.size()).to.equal(2);
    expect((await exampleIterableMapping.get(wallet1.address)).amount).to.equal(0);
    expect((await exampleIterableMapping.get(wallet1.address)).created_at).to.equal(0);
    expect((await exampleIterableMapping.get(wallet1.address)).updated_at).to.equal(0);
  });

  it('try to remove non-existent value', async () => {
    await exampleIterableMapping.set(wallet2.address, aTestWarranty);

    await exampleIterableMapping.remove(wallet1.address);

    const warranty = await exampleIterableMapping.get(wallet2.address);

    expect(warranty.amount).to.equal(aTestWarranty.amount);
    expect(warranty.created_at).to.equal(aTestWarranty.created_at);
    expect(warranty.updated_at).to.equal(aTestWarranty.updated_at);
    expect(await exampleIterableMapping.size()).to.equal(1);
  });

  it('iterate', async () => {
    await exampleIterableMapping.set(owner.address, aTestWarranty);
    await exampleIterableMapping.set(wallet1.address, aTestWarranty);
    await exampleIterableMapping.set(wallet2.address, aTestWarranty);

    await exampleIterableMapping.iterable();

    expect((await exampleIterableMapping.iterable()).length).to.equal(3);
  });
});
