import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { FakeCreditOracle, FakeToken, FakeXscrowV2 } from '../typechain-types';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

describe('FakeXscrowV2', () => {
  let fakeXscrowV2: FakeXscrowV2;
  let owner: SignerWithAddress;
  let wallet1: SignerWithAddress;
  let lenderTreasury: SignerWithAddress;
  let vendorTreasury: SignerWithAddress;
  let libIterableMapping: any;
  let oracle: FakeCreditOracle;
  let fakeToken: FakeToken;

  const testFee = 2;
  const aDummyValue = 3;
  const ownableRejectedMsg = 'Ownable: caller is not the owner';
  const testIdentifier = 'token_xscrow';
  const minimumDepositAmount = 1000000;
  const maximumDepositAmount = 10000000;

  beforeEach(async () => {
    [owner, wallet1, lenderTreasury, vendorTreasury] = await ethers.getSigners();

    fakeToken = await (await ethers.getContractFactory('FakeToken')).deploy('TokenX', 'TKX');
    libIterableMapping = await (await ethers.getContractFactory('IterableMapping')).deploy();
    oracle = await (await ethers.getContractFactory('FakeCreditOracle')).deploy();

    fakeXscrowV2 = (await upgrades.deployProxy(
      await ethers.getContractFactory('FakeXscrowV2', {
        libraries: {
          IterableMapping: libIterableMapping.address,
        },
      }),
      [
        fakeToken.address,
        lenderTreasury.address,
        vendorTreasury.address,
        testIdentifier,
        oracle.address,
        minimumDepositAmount,
        maximumDepositAmount,
      ],
      { initializer: 'initialize', kind: 'uups', unsafeAllowLinkedLibraries: true }
    )) as FakeXscrowV2;

    await fakeXscrowV2.updateDepositFee(testFee);
  });

  it('owner set dummy', async () => {
    await fakeXscrowV2.setDummy(aDummyValue);

    expect(await fakeXscrowV2.dummy()).to.equal(aDummyValue);
  });

  it('only owner can set dummy', async () => {
    await expect(fakeXscrowV2.connect(wallet1).setDummy(aDummyValue)).to.be.rejectedWith(ownableRejectedMsg);
  });
});
