// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "forge-std/Test.sol";
import "../contracts/fakes/FakeXscrow.sol";
import "../contracts/ManualOracle.sol";

contract ManualOracleTest is Test {
    event XscrowUpdated(address indexed aXscrow);
    event RequestPartialWithdrawOf(address indexed aPayee, uint256 anAmount);
    event DataFulfilled(address indexed aPayee, bool allowed);
    event RequestWithdrawOf(address indexed aPayee, uint256 anAmount);
    event RequestExecuteDepositOf(address indexed aPayee, uint256 anAmount);

    ManualOracle manualOracle;
    FakeXscrow fakeXscrow;
    address wallet1 = address(1);
    address wallet2 = address(2);
    uint256 withdrawAmount = 1000;

    function setUp() public {
        manualOracle = new ManualOracle();
        fakeXscrow = new FakeXscrow(address(wallet1));
    }

    function test_updateXscrow() public {
        manualOracle.updateXscrow(fakeXscrow);
        assertEq(manualOracle.xscrow(), address(fakeXscrow));
    }

    function test_revertWithCallerIsNotTheOwner_updateXscrow() public {
        vm.startPrank(wallet1);
        vm.expectRevert("Ownable: caller is not the owner");
        manualOracle.updateXscrow(fakeXscrow);
    }

    function test_revertWithAddressZeroNotAllowed_updateXscrow() public {
        vm.expectRevert(ManualOracle.AddressZeroNotAllowed.selector);
        manualOracle.updateXscrow(FakeXscrow(address(0)));
    }

    function test_emitUpdateXscrow() public {
        vm.expectEmit(true, false, false, false);
        emit XscrowUpdated(address(fakeXscrow));
        manualOracle.updateXscrow(fakeXscrow);
    }

    function test_requestPartialWithdraw() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.prank(address(fakeXscrow));
        manualOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);

        assertEq(manualOracle.withdrawRequestOf(wallet1), withdrawAmount);
    }

    function test_emitRequestPartialWithdrawOf() public {
        manualOracle.updateXscrow(fakeXscrow);
        vm.expectEmit();
        emit RequestPartialWithdrawOf(wallet1, withdrawAmount);

        vm.prank(address(fakeXscrow));
        manualOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);
    }

    function test_revertWithAlreadyRequested_requestPartialWithdrawOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.startPrank(address(fakeXscrow));
        manualOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);
        vm.expectRevert(ManualOracle.AlreadyRequested.selector);
        manualOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);
    }

    function test_revertWithNotXscrow_requestPartialWithdrawOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.startPrank(address(wallet2));
        vm.expectRevert(ManualOracle.NotXscrow.selector);
        manualOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);
    }

    function test_revertWithRequestNotFound_fulfillPartialWithdraw() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.expectRevert(ManualOracle.RequestNotFound.selector);
        manualOracle.fulfillPartialWithdraw(wallet1, true);
    }

    function test_revertWithCallerIsNotTheOwner_fulfillPartialWithdraw()
        public
    {
        manualOracle.updateXscrow(fakeXscrow);

        vm.expectRevert("Ownable: caller is not the owner");
        vm.prank(wallet1);
        manualOracle.fulfillPartialWithdraw(wallet1, true);
    }

    function test_emitDataFulfilled_fulfillPartialWithdraw() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.prank(address(fakeXscrow));
        manualOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);

        vm.expectEmit();
        emit DataFulfilled(wallet1, true);

        manualOracle.fulfillPartialWithdraw(wallet1, true);
    }

    function test_withdrawRequestOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.prank(address(fakeXscrow));
        manualOracle.requestPartialWithdrawOf(wallet1, withdrawAmount);

        assertEq(manualOracle.withdrawRequestOf(wallet1), withdrawAmount);
    }

    function test_revertWithCallerIsNotTheOwner_withdrawRequestOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.expectRevert("Ownable: caller is not the owner");
        vm.prank(wallet1);
        manualOracle.withdrawRequestOf(wallet1);
    }

    function test_withdrawRequestOfWithZeroBalance() public {
        manualOracle.updateXscrow(fakeXscrow);

        assertEq(manualOracle.withdrawRequestOf(wallet1), 0);
    }

    function test_requestWithdrawOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.prank(address(fakeXscrow));
        manualOracle.requestWithdrawOf(wallet1, withdrawAmount);

        assertEq(manualOracle.withdrawRequestOf(wallet1), withdrawAmount);
    }

    function test_emitRequestWithdrawOf() public {
        manualOracle.updateXscrow(fakeXscrow);
        vm.expectEmit();
        emit RequestWithdrawOf(wallet1, withdrawAmount);

        vm.prank(address(fakeXscrow));
        manualOracle.requestWithdrawOf(wallet1, withdrawAmount);
    }

    function test_revertWithAlreadyRequested_requestWithdrawOf() public {
        manualOracle.updateXscrow(fakeXscrow);
        vm.startPrank(address(fakeXscrow));
        manualOracle.requestWithdrawOf(wallet1, withdrawAmount);
        vm.expectRevert(ManualOracle.AlreadyRequested.selector);
        manualOracle.requestWithdrawOf(wallet1, withdrawAmount);
    }

    function test_revertWithNotXscrow_requestWithdrawOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.startPrank(address(wallet2));
        vm.expectRevert(ManualOracle.NotXscrow.selector);
        manualOracle.requestWithdrawOf(wallet1, withdrawAmount);
    }

    function test_revertWithRequestNotFound_fulfillWithdraw() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.expectRevert(ManualOracle.RequestNotFound.selector);
        manualOracle.fulfillWithdraw(wallet1, true);
    }

    function test_revertWithCallerIsNotTheOwner_fulfillWithdraw() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.expectRevert("Ownable: caller is not the owner");
        vm.prank(wallet1);
        manualOracle.fulfillWithdraw(wallet1, true);
    }

    function test_emitDataFulfilled_fulfillWithdraw() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.prank(address(fakeXscrow));
        manualOracle.requestWithdrawOf(wallet1, withdrawAmount);

        vm.expectEmit();
        emit DataFulfilled(wallet1, true);

        manualOracle.fulfillWithdraw(wallet1, true);
    }

    function test_revertWithCallerIsNotTheOwner_executionRequestOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.expectRevert("Ownable: caller is not the owner");
        vm.prank(wallet1);
        manualOracle.executionRequestOf(wallet1);
    }

    function test_executionRequestOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.prank(address(fakeXscrow));
        manualOracle.requestExecuteDepositOf(wallet1, withdrawAmount);

        assertEq(manualOracle.executionRequestOf(wallet1), withdrawAmount);
    }

    function test_executionRequestOfWithZeroBalance() public {
        manualOracle.updateXscrow(fakeXscrow);

        assertEq(manualOracle.executionRequestOf(wallet1), 0);
    }

    function test_requestExecuteDepositOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.prank(address(fakeXscrow));
        manualOracle.requestExecuteDepositOf(wallet1, withdrawAmount);

        assertEq(manualOracle.executionRequestOf(wallet1), withdrawAmount);
    }

    function test_emitRequestExecuteDepositOf() public {
        manualOracle.updateXscrow(fakeXscrow);
        vm.expectEmit();
        emit RequestExecuteDepositOf(wallet1, withdrawAmount);

        vm.prank(address(fakeXscrow));
        manualOracle.requestExecuteDepositOf(wallet1, withdrawAmount);
    }

    function test_revertWithAlreadyRequested_requestExecuteDepositOf() public {
        manualOracle.updateXscrow(fakeXscrow);
        vm.startPrank(address(fakeXscrow));
        manualOracle.requestExecuteDepositOf(wallet1, withdrawAmount);
        vm.expectRevert(ManualOracle.AlreadyRequested.selector);
        manualOracle.requestExecuteDepositOf(wallet1, withdrawAmount);
    }

    function test_revertWithNotXscrow_requestExecuteDepositOf() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.startPrank(address(wallet2));
        vm.expectRevert(ManualOracle.NotXscrow.selector);
        manualOracle.requestExecuteDepositOf(wallet1, withdrawAmount);
    }

    function test_revertWithRequestNotFound_fulfillExecution() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.expectRevert(ManualOracle.RequestNotFound.selector);
        manualOracle.fulfillExecution(wallet1, true);
    }

    function test_emitDataFulfilled_fulfillExecution() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.prank(address(fakeXscrow));
        manualOracle.requestExecuteDepositOf(wallet1, withdrawAmount);

        vm.expectEmit();
        emit DataFulfilled(wallet1, true);

        manualOracle.fulfillExecution(wallet1, true);
    }

    function test_revertWithCallerIsNotTheOwner_fulfillExecution() public {
        manualOracle.updateXscrow(fakeXscrow);

        vm.expectRevert("Ownable: caller is not the owner");
        vm.prank(wallet1);
        manualOracle.fulfillExecution(wallet1, true);
    }
}
