import { expect } from 'chai';
import DefaultEnvironment from '../environment/default/default-environment';
import { rawEnvironmentData } from '../environment/raw/raw-environment-data';
import { Environment } from '../environment/environment.interface';

describe('DefaultEnvironment', () => {
  let defaultEnv: Environment;

  beforeEach(() => {
    defaultEnv = new DefaultEnvironment(rawEnvironmentData);
  });

  it('linkTokenAddress', () => {
    expect(defaultEnv.linkTokenAddress()).to.equal(rawEnvironmentData.LINK_TOKEN_ADDRESS);
  });

  it('operatorAddress', () => {
    expect(defaultEnv.operatorAddress()).to.equal(rawEnvironmentData.OPERATOR_ADDRESS);
  });

  it('jobId', () => {
    expect(defaultEnv.jobId()).to.equal(rawEnvironmentData.JOB_ID);
  });

  it('withdrawApiUrl', () => {
    expect(defaultEnv.withdrawApiUrl()).to.equal(rawEnvironmentData.WITHDRAW_API_URL);
  });

  it('executionApiUrl', () => {
    expect(defaultEnv.executionApiUrl()).to.equal(rawEnvironmentData.EXECUTION_API_URL);
  });

  it('tokenAddress', () => {
    expect(defaultEnv.tokenAddress()).to.equal(rawEnvironmentData.TOKEN_ADDRESS);
  });

  it('lenderTreasuryAddress', () => {
    expect(defaultEnv.lenderTreasuryAddress()).to.equal(rawEnvironmentData.LENDER_TREASURY_ADDRESS);
  });

  it('vendorTreasuryAddress', () => {
    expect(defaultEnv.vendorTreasuryAddress()).to.equal(rawEnvironmentData.VENDOR_TREASURY_ADDRESS);
  });

  it('xscrowId', () => {
    expect(defaultEnv.xscrowId()).to.equal(rawEnvironmentData.XSCROW_ID);
  });

  it('oracleAddress', () => {
    expect(defaultEnv.oracleAddress()).to.equal(rawEnvironmentData.ORACLE_ADDRESS);
  });

  it('minDepositAmount', () => {
    expect(defaultEnv.minDepositAmount()).to.equal(rawEnvironmentData.MIN_DEPOSIT_AMOUNT);
  });

  it('maxDepositAmount', () => {
    expect(defaultEnv.maxDepositAmount()).to.equal(rawEnvironmentData.MAX_DEPOSIT_AMOUNT);
  });

  it('mnemonic', () => {
    expect(defaultEnv.mnemonic()).to.equal(rawEnvironmentData.MNEMONIC);
  });
});
