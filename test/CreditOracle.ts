import { expect } from 'chai';
import { ethers } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { CreditOracle, FakeOperator, FakeXscrow, LinkToken } from '../typechain-types';

describe('Credit Oracle', () => {
  ethers.utils.Logger.setLogLevel(ethers.utils.Logger.levels.ERROR);
  let jobId: Uint8Array;
  let oracle: CreditOracle;
  let fakeXscrow: FakeXscrow;
  let owner: SignerWithAddress;
  let fakeLinkToken: LinkToken;
  let wallet1: SignerWithAddress;
  let fakeOperator: FakeOperator;

  const linkAmount = '1000000000000000000';
  const xscrowRejectedMsg = 'Caller is not the xscrow';
  const ownableRejectedMsg = 'Ownable: caller is not the owner';
  const operatorRejectedMsg = 'Source must be the oracle of the request';
  const zeroAddress = ethers.constants.AddressZero;
  const zeroAddressMsg = 'Address can not be zero';
  const aWithdrawApiUrl = 'https://anurl.com?address=';
  const anExecutionApiUrl = 'https://an-execution-url.com?address=';
  const partialWithdrawAmount = 1000;
  const executionAmount = 1000;

  beforeEach(async () => {
    jobId = ethers.utils.toUtf8Bytes('7da2702f37fd48e5b1b9a5715e3509b6');
    [owner, wallet1] = await ethers.getSigners();
    fakeLinkToken = await (await ethers.getContractFactory('LinkToken')).deploy();

    fakeOperator = await (await ethers.getContractFactory('FakeOperator')).deploy(fakeLinkToken.address);

    oracle = await (
      await ethers.getContractFactory('CreditOracle')
    ).deploy(fakeLinkToken.address, fakeOperator.address, jobId, aWithdrawApiUrl, anExecutionApiUrl);

    fakeXscrow = await (await ethers.getContractFactory('FakeXscrow')).deploy(oracle.address);

    await oracle.updateXscrow(fakeXscrow.address);

    await fakeLinkToken.connect(owner).transfer(oracle.address, linkAmount);
  });

  describe('deploy settings', () => {
    it('new', async () => {
      expect(oracle).to.not.be.null;
    });

    it.skip('operator with zero address', async () => {
      await expect(
        (
          await ethers.getContractFactory('CreditOracle')
        ).deploy(fakeLinkToken.address, zeroAddress, jobId, aWithdrawApiUrl, anExecutionApiUrl)
      ).to.revertedWith(zeroAddressMsg);
    });

    it.skip('link token with zero address', async () => {
      await expect(
        (
          await ethers.getContractFactory('CreditOracle')
        ).deploy(zeroAddress, fakeOperator.address, jobId, aWithdrawApiUrl, anExecutionApiUrl)
      ).to.revertedWith(zeroAddressMsg);
    });
  });

  describe('request oracle', () => {
    it('request withdraw of', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestWithdrawOf(wallet1.address, partialWithdrawAmount);
      const transactionReceipt = await transaction.wait(1);
      const requestId = transactionReceipt.events![1].topics[1];

      expect(requestId).to.not.be.null;
    });

    it.skip('request withdraw of event', async () => {
      await oracle.updateXscrow(owner.address);
      await new Promise(async (resolve) => {
        const filter = oracle.filters.RequestWithdrawOf(wallet1.address);
        oracle.once(filter, (aPayee: any, anAmount: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(partialWithdrawAmount).to.equal(anAmount);
          resolve(true);
        });
        await (await oracle.requestWithdrawOf(wallet1.address, partialWithdrawAmount)).wait(1);
      });
    });

    it.skip('request withdraw of from no xscrow', async () => {
      await expect(oracle.requestWithdrawOf(wallet1.address, partialWithdrawAmount)).rejectedWith(xscrowRejectedMsg);
    });

    it('request partial withdraw of', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestPartialWithdrawOf(wallet1.address, 1000);
      const transactionReceipt = await transaction.wait(1);
      const requestId = transactionReceipt.events![1].topics[1];

      expect(requestId).to.not.be.null;
    });

    it.skip('request partial withdraw of event', async () => {
      await oracle.updateXscrow(owner.address);
      await new Promise(async (resolve) => {
        const filter = oracle.filters.RequestPartialWithdrawOf(wallet1.address);
        oracle.once(filter, (aPayee: any, anAmount: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(anAmount).to.equal(1000);
          resolve(true);
        });
        await (await oracle.requestPartialWithdrawOf(wallet1.address, 1000)).wait(1);
      });
    });

    it.skip('request partial withdraw from no xscrow', async () => {
      await expect(oracle.requestPartialWithdrawOf(wallet1.address, 1000)).rejectedWith(xscrowRejectedMsg);
    });

    it('fulfill call from no operator', async () => {
      await expect(oracle.fulfillWithdraw(ethers.utils.defaultAbiCoder.encode(['bool'], [true]), true)).rejectedWith(
        operatorRejectedMsg
      );
    });

    it.skip('request fulfill through fake oracle from xscrow', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.DataFulfilled(wallet1.address);
        oracle.once(filter, (aPayee: any, allowed: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(allowed).to.equal(true);
          resolve(true);
        });

        const transactionReceipt = await (await fakeXscrow.connect(wallet1).requestWithdraw()).wait(1);
        await fakeOperator.fulfillOracleRequest(
          transactionReceipt.events![1].topics[1],
          ethers.utils.defaultAbiCoder.encode(['bool'], [true])
        );
      });
    });

    it.skip('request partial fulfill through fake oracle from xscrow', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.DataFulfilled(wallet1.address);
        oracle.once(filter, (aPayee: any, allowed: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(allowed).to.equal(true);
          resolve(true);
        });

        const transactionReceipt = await (await fakeXscrow.connect(wallet1).requestPartialWithdraw(1000)).wait(1);
        await fakeOperator.fulfillOracleRequest(
          transactionReceipt.events![1].topics[1],
          ethers.utils.defaultAbiCoder.encode(['bool'], [true])
        );
      });
    });

    it('request execute deposit of', async () => {
      await oracle.updateXscrow(owner.address);
      const transaction = await oracle.requestExecuteDepositOf(wallet1.address, executionAmount);
      const transactionReceipt = await transaction.wait(1);
      const requestId = transactionReceipt.events![1].topics[1];

      expect(requestId).to.not.be.null;
    });

    it.skip('request execute deposit from no xscrow', async () => {
      await expect(oracle.requestExecuteDepositOf(wallet1.address, executionAmount)).rejectedWith(xscrowRejectedMsg);
    });

    it.skip('request execute deposit event', async () => {
      await oracle.updateXscrow(owner.address);
      await new Promise(async (resolve) => {
        const filter = oracle.filters.RequestExecuteDepositOf(wallet1.address);
        oracle.once(filter, (aPayee: any, anAmount: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(executionAmount).to.equal(anAmount);
          resolve(true);
        });
        await (await oracle.requestExecuteDepositOf(wallet1.address, executionAmount)).wait(1);
      });
    });

    it('fulfillExecution call from no operator', async () => {
      await expect(oracle.fulfillExecution(ethers.utils.defaultAbiCoder.encode(['bool'], [true]), true)).rejectedWith(
        operatorRejectedMsg
      );
    });

    it.skip('request fulfillExecution through fake oracle from xscrow', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.DataFulfilled(wallet1.address);
        oracle.once(filter, (aPayee: any, allowed: any) => {
          expect(wallet1.address).to.equal(aPayee);
          expect(allowed).to.equal(true);
          resolve(true);
        });

        const transactionReceipt = await (await fakeXscrow.requestExecuteDepositOf(wallet1.address)).wait(1);
        await fakeOperator.fulfillOracleRequest(
          transactionReceipt.events![1].topics[1],
          ethers.utils.defaultAbiCoder.encode(['bool'], [true])
        );
      });
    });
  });

  describe('updatable properties', () => {
    it('owner can update xscrow address', async () => {
      await oracle.updateXscrow(wallet1.address);
      expect(await oracle.xscrow()).to.equal(wallet1.address);
    });

    it('not owner cannot update xscrow', async () => {
      await expect(oracle.connect(wallet1).updateXscrow(wallet1.address)).to.revertedWith(ownableRejectedMsg);
    });

    it.skip('update xscrow event', async () => {
      await new Promise(async (resolve) => {
        const filter = oracle.filters.XscrowUpdated(wallet1.address);
        oracle.once(filter, (aXscrow: any) => {
          expect(wallet1.address).to.equal(aXscrow);
          resolve(true);
        });

        await oracle.updateXscrow(wallet1.address);
      });
    });

    it.skip('not zero address on update xscrow', async () => {
      await expect(oracle.updateXscrow(zeroAddress)).to.revertedWith(zeroAddressMsg);
    });

    it('owner can update api url', async () => {
      await oracle.updateWithdrawApiUrl(aWithdrawApiUrl);

      expect(await oracle.withdrawApiUrl()).to.equal(aWithdrawApiUrl);
    });

    it('not owner cannot update withdraw api url', async () => {
      await expect(oracle.connect(wallet1).updateWithdrawApiUrl(aWithdrawApiUrl)).to.revertedWith(ownableRejectedMsg);
    });

    it('not owner cannot read api url', async () => {
      await expect(oracle.connect(wallet1).withdrawApiUrl()).to.revertedWith(ownableRejectedMsg);
    });

    it('owner can update execution url', async () => {
      await oracle.updateExecutionApiUrl(anExecutionApiUrl);

      expect(await oracle.executionApiUrl()).to.equal(anExecutionApiUrl);
    });

    it('not owner cannot update execution url', async () => {
      await expect(oracle.connect(wallet1).updateExecutionApiUrl(anExecutionApiUrl)).to.revertedWith(
        ownableRejectedMsg
      );
    });

    it('not owner cannot read execution url', async () => {
      await expect(oracle.connect(wallet1).executionApiUrl()).to.revertedWith(ownableRejectedMsg);
    });

    it('owner can update job id', async () => {
      const jobIdString = ethers.utils.toUtf8Bytes('7da2702f37fd48e5b1b9a5715e350943');
      const newJobId = ethers.utils.keccak256(jobIdString);

      await oracle.updateJobId(newJobId);

      expect(await oracle.jobId()).to.equal(newJobId);
    });

    it('not owner cannot update job id', async () => {
      const jobIdString = ethers.utils.toUtf8Bytes('7da2702f37fd48e5b1b9a5715e350943');
      const newJobId = ethers.utils.keccak256(jobIdString);

      await expect(oracle.connect(wallet1).updateJobId(newJobId)).to.revertedWith(ownableRejectedMsg);
    });

    it('not owner cannot read job id', async () => {
      await expect(oracle.connect(wallet1).jobId()).to.revertedWith(ownableRejectedMsg);
    });

    it('owner can update operator address', async () => {
      await oracle.updateOperator(wallet1.address);

      expect(await oracle.operator()).to.equal(wallet1.address);
    });

    it('not owner cannot update operator address', async () => {
      await expect(oracle.connect(wallet1).updateOperator(wallet1.address)).to.revertedWith(ownableRejectedMsg);
    });

    it('not owner cannot read operator address', async () => {
      await expect(oracle.connect(wallet1).operator()).to.revertedWith(ownableRejectedMsg);
    });

    it.skip('not zero address on update operator', async () => {
      await expect(oracle.updateOperator(zeroAddress)).to.revertedWith(zeroAddressMsg);
    });
  });

  describe('link withdraw', () => {
    it('owner can withdraw link', async () => {
      const initialBalance = await fakeLinkToken.balanceOf(owner.address);

      await oracle.withdrawLink();

      expect(await fakeLinkToken.balanceOf(owner.address)).to.equal(initialBalance.add(linkAmount));
    });

    it('not owner cannot withdraw link', async () => {
      await expect(oracle.connect(wallet1).withdrawLink()).to.revertedWith(ownableRejectedMsg);
    });
  });
});
