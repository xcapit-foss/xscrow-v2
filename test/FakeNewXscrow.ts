import { expect } from 'chai';
import { ethers } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { FakeNewXscrow } from '../typechain-types';

describe('Fake New Xscrow', () => {
  ethers.utils.Logger.setLogLevel(ethers.utils.Logger.levels.ERROR);
  let wallet1: SignerWithAddress;
  let fakeNewXscrow: FakeNewXscrow;

  beforeEach(async () => {
    [wallet1] = await ethers.getSigners();
    fakeNewXscrow = await (await ethers.getContractFactory('FakeNewXscrow')).deploy();
  });

  it('new', async () => {
    expect(fakeNewXscrow).to.not.be.null;
  });

  it('importWarranty', async () => {
    const aWarranty = { amount: 1, created_at: 2, updated_at: 3 };

    await fakeNewXscrow.restore(aWarranty, wallet1.address);

    expect(true).to.equal(true);
  });

  it('balanceOf', async () => {
    const aWarranty = { amount: 1, created_at: 2, updated_at: 3 };

    await fakeNewXscrow.restore(aWarranty, wallet1.address);

    expect(await fakeNewXscrow.balanceOf(wallet1.address)).to.equal(1);
  });
});
