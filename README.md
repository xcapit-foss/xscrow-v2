# Xscrow V2

The Xscrow contract allows deposits in the configured token, and a Lender uses that information to grant credit in fiat currency. When an address wants to withdraw their deposit, they communicate with the Lender through an Oracle to verify if the operation is permitted. If an address does not comply with their credit obligations, the Lender can initiate the execution of that deposit, and an Oracle is consulted to verify if the operation is allowed.

## Community

- [Discord](https://discord.gg/AnGXcZ8P)
- [Code of Conduct](https://xcapit-foss.gitlab.io/documentation/docs/CODE_OF_CONDUCT)
- [Contribution Guideline](https://xcapit-foss.gitlab.io/documentation/docs/contribution_guidelines)
- [Documentation](https://xcapit-foss.gitlab.io/documentation/docs/xscrow/xscrow/)

# TODO

- Migrar los tests faltantes a foundry.
