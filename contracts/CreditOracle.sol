// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@chainlink/contracts/src/v0.8/ChainlinkClient.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "./interfaces/IXscrow.sol";
import "./interfaces/Oracle.sol";
import "./structures/Request.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract CreditOracle is ChainlinkClient, Ownable, ReentrancyGuard, Oracle {
    using Chainlink for Chainlink.Request;

    LinkTokenInterface private immutable _linkToken;
    address private _operator;
    IXscrow private _xscrow;
    bytes32 private _jobId;
    uint256 private immutable fee;
    string private _withdrawApiUrl;
    string private _executionApiUrl;
    mapping(bytes32 => Request) private _requests;

    event DataFulfilled(address indexed aPayee, bool allowed);
    event RequestWithdrawOf(address indexed aPayee, uint256 anAmount);
    event RequestPartialWithdrawOf(address indexed aPayee, uint256 anAmount);
    event RequestExecuteDepositOf(address indexed aPayee, uint256 anAmount);
    event XscrowUpdated(address indexed aXscrow);

    error NotXscrow();
    error AddressZeroNotAllowed();

    constructor(
        LinkTokenInterface linkToken_,
        address operator_,
        bytes32 jobId_,
        string memory aWithdrawApiUrl_,
        string memory anExecutionApiUrl_
    ) nonZeroAddress(operator_) nonZeroAddress(address(linkToken_)) {
        _linkToken = linkToken_;
        _operator = operator_;
        _jobId = jobId_;
        _withdrawApiUrl = aWithdrawApiUrl_;
        _executionApiUrl = anExecutionApiUrl_;
        setChainlinkToken(address(_linkToken));
        fee = (1 * LINK_DIVISIBILITY) / 10;
    }

    modifier onlyXscrow() {
        if (msg.sender != address(_xscrow)) revert NotXscrow();
        _;
    }

    modifier nonZeroAddress(address anAddress) {
        if (anAddress == address(0)) revert AddressZeroNotAllowed();
        _;
    }

    function xscrow() external view returns (address) {
        return address(_xscrow);
    }

    function withdrawApiUrl() external view onlyOwner returns (string memory) {
        return _withdrawApiUrl;
    }

    function executionApiUrl() external view onlyOwner returns (string memory) {
        return _executionApiUrl;
    }

    function jobId() external view onlyOwner returns (bytes32) {
        return _jobId;
    }

    function operator() external view onlyOwner returns (address) {
        return _operator;
    }

    function requestExecuteDepositOf(
        address aPayee,
        uint256 anAmount
    ) external onlyXscrow nonReentrant {
        emit RequestExecuteDepositOf(aPayee, anAmount);
        bytes32 reqId = _chainlinkRequest(
            this.fulfillExecution.selector,
            _url(aPayee, _executionApiUrl)
        );

        _requests[reqId] = Request(aPayee, anAmount);
    }

    function fulfillExecution(
        bytes32 requestId,
        bool canExecute
    ) public recordChainlinkFulfillment(requestId) {
        Request memory request = _requests[requestId];
        delete _requests[requestId];
        emit DataFulfilled(request.payee, canExecute);
        _xscrow.executeDepositOf(request.payee, canExecute);
    }

    function _chainlinkRequest(
        bytes4 aCallbackFunctionSignature_,
        string memory anUrl_
    ) private returns (bytes32 requestId) {
        Chainlink.Request memory req = buildChainlinkRequest(
            _jobId,
            address(this),
            aCallbackFunctionSignature_
        );

        req.add("get", anUrl_);
        req.add("path", "allowed");

        return sendChainlinkRequestTo(_operator, req, fee);
    }

    function requestWithdrawOf(
        address aPayee,
        uint256 anAmount
    ) external onlyXscrow nonReentrant {
        emit RequestWithdrawOf(aPayee, anAmount);

        bytes32 reqId = _chainlinkRequest(
            this.fulfillWithdraw.selector,
            _url(aPayee, _withdrawApiUrl)
        );
        _requests[reqId] = Request(aPayee, anAmount);
    }

    function requestPartialWithdrawOf(
        address aPayee,
        uint256 anAmount
    ) external onlyXscrow nonReentrant {
        emit RequestPartialWithdrawOf(aPayee, anAmount);
        bytes32 reqId = _chainlinkRequest(
            this.fulfillPartialWithdraw.selector,
            _urlWithAmountOf(_url(aPayee, _withdrawApiUrl), anAmount)
        );
        _requests[reqId] = Request(aPayee, anAmount);
    }

    function _urlWithAmountOf(
        string memory anUrl_,
        uint256 anAmount_
    ) private pure returns (string memory) {
        return
            string(
                abi.encodePacked(
                    anUrl_,
                    "&amount=",
                    Strings.toString(anAmount_)
                )
            );
    }

    function _url(
        address anAddress_,
        string memory anApiUrl_
    ) private pure returns (string memory) {
        return
            string(
                abi.encodePacked(
                    anApiUrl_,
                    Strings.toHexString(uint256(uint160(anAddress_)), 20)
                )
            );
    }

    function fulfillPartialWithdraw(
        bytes32 requestId,
        bool canWithdraw
    ) public recordChainlinkFulfillment(requestId) {
        Request memory request = _requests[requestId];
        delete _requests[requestId];
        emit DataFulfilled(request.payee, canWithdraw);
        _xscrow.partialWithdrawOf(request.payee, request.amount, canWithdraw);
    }

    function fulfillWithdraw(
        bytes32 requestId,
        bool canWithdraw
    ) public recordChainlinkFulfillment(requestId) {
        Request memory request = _requests[requestId];
        delete _requests[requestId];
        emit DataFulfilled(request.payee, canWithdraw);
        _xscrow.withdrawOf(request.payee, canWithdraw);
    }

    function withdrawLink() external onlyOwner returns (bool success) {
        return
            _linkToken.transfer(
                msg.sender,
                _linkToken.balanceOf(address(this))
            );
    }

    function updateWithdrawApiUrl(string memory aUrl) external onlyOwner {
        _withdrawApiUrl = aUrl;
    }

    function updateExecutionApiUrl(string memory aUrl) external onlyOwner {
        _executionApiUrl = aUrl;
    }

    function updateXscrow(
        IXscrow aXscrow
    ) external onlyOwner nonZeroAddress(address(aXscrow)) {
        emit XscrowUpdated(address(aXscrow));
        _xscrow = aXscrow;
    }

    function updateJobId(bytes32 aJobId) external onlyOwner {
        _jobId = aJobId;
    }

    function updateOperator(
        address anOperator
    ) external onlyOwner nonZeroAddress(anOperator) {
        _operator = anOperator;
    }
}
