// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

interface IXscrow {
    function withdrawOf(address aPayee, bool canWithdraw) external;

    function partialWithdrawOf(
        address aPayee,
        uint256 anAmount,
        bool canWithdraw
    ) external;

    function executeDepositOf(address aPayee, bool canExecute) external;
}
