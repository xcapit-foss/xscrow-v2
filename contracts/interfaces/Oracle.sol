// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

interface Oracle {
    function requestWithdrawOf(address aPayee, uint256 anAmount) external;

    function requestPartialWithdrawOf(
        address aPayee,
        uint256 anAmount
    ) external;

    function requestExecuteDepositOf(address aPayee, uint256 anAmount) external;
}
