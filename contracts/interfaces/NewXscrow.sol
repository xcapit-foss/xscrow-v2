// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "../structures/Warranty.sol";

interface NewXscrow {
    function restore(Warranty memory aWarranty, address aPayee) external;
}
