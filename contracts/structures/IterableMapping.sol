// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.9;


import "../structures/Warranty.sol";


library IterableMapping {

    struct AddressToWarranty {
        address[] keys;
        mapping(address => Warranty) values;
        mapping(address => uint) indexOf;
        mapping(address => bool) inserted;
    }

    function set(
        AddressToWarranty storage addressToWarranty,
        address anAddress,
        Warranty memory aWarranty
    ) public {
        if (!addressToWarranty.inserted[anAddress]) {
            addressToWarranty.inserted[anAddress] = true;
            addressToWarranty.indexOf[anAddress] = addressToWarranty
                .keys
                .length;
            addressToWarranty.keys.push(anAddress);

        }
        addressToWarranty.values[anAddress] = aWarranty;
    }

    function get(
        AddressToWarranty storage addressToWarranty,
        address anAddress
    ) public view returns (Warranty memory) {
        return addressToWarranty.values[anAddress];
    }

    function size(
        AddressToWarranty storage addressToWarranty
    ) public view returns (uint) {
        return addressToWarranty.keys.length;
    }

    function getKeyAtIndex(
        AddressToWarranty storage addressToWarranty,
        uint index
    ) public view returns (address) {
        return addressToWarranty.keys[index];
    }

    function remove(
        AddressToWarranty storage addressToWarranty,
        address anAddress
    ) public {
        if (addressToWarranty.inserted[anAddress]) {
            delete addressToWarranty.inserted[anAddress];
            delete addressToWarranty.values[anAddress];
            _reArrangeIndexes(addressToWarranty, anAddress);
        }
    }

    function _reArrangeIndexes(
        AddressToWarranty storage addressToWarranty,
        address anAddress
    ) private {
        uint index = addressToWarranty.indexOf[anAddress];
        address lastKey = addressToWarranty.keys[
            addressToWarranty.keys.length - 1
        ];

        addressToWarranty.indexOf[lastKey] = index;
        delete addressToWarranty.indexOf[anAddress];

        addressToWarranty.keys[index] = lastKey;
        addressToWarranty.keys.pop();
    }
}
