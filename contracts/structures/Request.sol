// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

struct Request {
    address payee;
    uint256 amount;
}
