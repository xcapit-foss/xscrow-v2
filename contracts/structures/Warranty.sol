// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

struct Warranty {
    uint256 amount;
    uint256 created_at;
    uint256 updated_at;
}
