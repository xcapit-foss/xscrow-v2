// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "../Xscrow.sol";

contract FakeXscrowV2 is Xscrow {

    uint256 _dummy;

    function dummy() external view returns (uint256) {
      return _dummy;
    }

    function setDummy(uint256 aDummyValue) external onlyOwner {
      _dummy = aDummyValue;
    }
}
