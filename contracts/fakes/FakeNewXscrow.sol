// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "../interfaces/NewXscrow.sol";
import "../structures/Warranty.sol";

contract FakeNewXscrow is NewXscrow {
    mapping(address => Warranty) private _warranties;

    function restore(Warranty memory aWarranty, address aPayee) public {
        _warranties[aPayee] = aWarranty;
    }

    function balanceOf(address aPayee) external view returns (uint256) {
        return _warranties[aPayee].amount;
    }
}
