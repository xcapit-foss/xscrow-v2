// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

interface ERC677Receiver {
    function onTokenTransfer(
        address _sender,
        uint _value,
        bytes memory _data
    ) external;
}

contract LinkToken is ERC20 {
    constructor() ERC20("Chaninlink", "LINK") {
        _mint(msg.sender, 100 * 10 ** uint(decimals()));
    }

    event Transfer(
        address indexed from,
        address indexed to,
        uint value,
        bytes data
    );

    function transferAndCall(
        address _to,
        uint _value,
        bytes memory _data
    ) public returns (bool success) {
        super.transfer(_to, _value);
        emit Transfer(msg.sender, _to, _value, _data);
        if (isContract(_to)) {
            ERC677Receiver(_to).onTokenTransfer(msg.sender, _value, _data);
        }
        return true;
    }

    function isContract(address _addr) private view returns (bool hasCode) {
        uint length;
        assembly {
            length := extcodesize(_addr)
        }
        return length > 0;
    }
}
