// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "../interfaces/Oracle.sol";

contract FakeCreditOracle is Oracle {
    bytes32 public requestCreditDataOfRequestId =
        "requestCreditDataOfRequestId";
    bytes32 public requestPartialWithdrawOfRequestId =
        "requestPartWithdrawOfRequestId";
    bytes32 public executeDataOfRequestId = "executeDataOfTestRequestId";
    event ChainlinkRequested(bytes32 indexed requestId);

    function requestWithdrawOf(address aPayee, uint256 anAmount) external {
        emit ChainlinkRequested(requestCreditDataOfRequestId);
    }

    function requestPartialWithdrawOf(
        address aPayee,
        uint256 anAmount
    ) external {
        emit ChainlinkRequested(requestPartialWithdrawOfRequestId);
    }

    function requestExecuteDepositOf(
        address aPayee,
        uint256 anAmount
    ) external {
        emit ChainlinkRequested(executeDataOfRequestId);
    }
}
