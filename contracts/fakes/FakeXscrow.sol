// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "../interfaces/Oracle.sol";
import "../interfaces/IXscrow.sol";

contract FakeXscrow is IXscrow {
    address private _oracle;
    event WithdrawSuccessful(address indexed aPayee, uint256 anAmount);
    event DepositExecuted(address indexed aPayee, uint256 balance);

    constructor(address oracle_) {
        _oracle = oracle_;
    }

    function requestWithdraw() external {
        Oracle(_oracle).requestWithdrawOf(msg.sender, 1);
    }

    function withdrawOf(address aPayee, bool canWithdraw) public {
        require(canWithdraw == true || canWithdraw == false);
        emit WithdrawSuccessful(aPayee, 1);
    }

    function partialWithdrawOf(
        address aPayee,
        uint256 anAmount,
        bool canWithdraw
    ) public {
        require(canWithdraw == true);
        emit WithdrawSuccessful(aPayee, anAmount);
    }

    function requestExecuteDepositOf(address aPayee) external {
        Oracle(_oracle).requestExecuteDepositOf(aPayee, 1);
    }

    function requestPartialWithdraw(uint256 anAmount) external {
        Oracle(_oracle).requestPartialWithdrawOf(msg.sender, anAmount);
    }

    function executeDepositOf(address aPayee, bool canExecute) public {
        require(canExecute == true || canExecute == false);
        emit DepositExecuted(aPayee, 5);
    }
}
