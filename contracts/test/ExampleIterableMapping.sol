// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.9;

import "../structures/IterableMapping.sol";
import "../structures/Warranty.sol";

contract ExampleIterableMapping {
    using IterableMapping for IterableMapping.AddressToWarranty;

    IterableMapping.AddressToWarranty private _values;

    function set(address anAddress, Warranty memory aWarranty) public {
        _values.set(anAddress, aWarranty);
    }

    function get(address anAddress) public view returns (Warranty memory) {
        return _values.get(anAddress);
    }

    function size() public view returns (uint) {
        return _values.size();
    }

    function getKeyAtIndex(uint index) public view returns (address) {
        return _values.getKeyAtIndex(index);
    }

    function remove(address anAddress) public {
        _values.remove(anAddress);
    }

    function iterable() public view returns (Warranty[] memory) {
        Warranty[] memory warranties = new Warranty[](_values.size());
        for (uint i = 0; i < _values.size(); i++) {
            warranties[i] = (_values.get(_values.getKeyAtIndex(i)));
        }
        return warranties;
    }
}
